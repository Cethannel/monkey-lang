mod lexer;
mod repl;
mod token;
mod ast;

fn main() {
    println!("Commands:");
    repl::start(std::io::stdin(), std::io::stdout())
}
