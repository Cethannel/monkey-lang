use crate::token::Token;

enum AstNode {
    Statement(Token),
    Expression,
}

impl AstNode {
    fn token_literal(&self) -> String {
        match self {
            AstNode::Statement(token) => token.token_literal(),
            AstNode::Expression => todo!()
        }
    }
}

struct Program {
    nodes: Vec<AstNode>
}

impl Program {
    fn token_literal(&self) -> String {
        if self.nodes.len() > 0 {
            self.nodes[0].token_literal()
        } else {
            "".to_string()
        }
    }
}
