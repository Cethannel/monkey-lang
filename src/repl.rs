use std::io::{prelude::*, Stdin, Stdout};

use crate::lexer;

const PROMPT: &'static str = ">> ";

pub fn start(rdr: Stdin, mut out: Stdout) {
    loop {
        write!(out, "{}", PROMPT).expect("Failed to write to out");
        out.lock().flush().expect("Failed to flush stdio");

        let mut buf = String::new();

        rdr.read_line(&mut buf).expect("Failed to read from rdr");

        let lexer = lexer::Lexer::new(buf);

        for tok in lexer {
            writeln!(out, "{:?}", tok).expect("Failed to write to out");
        }
    }
}
