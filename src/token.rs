use phf::{phf_map, Map};

#[allow(unused)]
#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Illegal(String),
    EOF,
    Ident(String),
    Int(String),
    /// =
    Assign,
    /// +
    Plus,
    /// -
    Minus,
    /// !
    Bang,
    /// *
    Asterisk,
    /// /
    Slash,
    /// <
    LT,
    /// >
    GT,
    /// ,
    Comma,
    /// ;
    Semicolon,
    /// (
    LParen,
    /// )
    RParen,
    /// {
    LBrace,
    /// }
    RBrace,
    /// ==
    Eq,
    /// !=
    NotEq,
    Function,
    Let,
    True,
    False,
    If,
    Else,
    Return,
}

impl Token {
    pub fn token_literal(&self) -> String {
        match self {
            Token::Illegal(literal) => literal,
            Token::EOF => "\0",
            Token::Ident(literal) => literal,
            Token::Int(literal) => literal,
            Token::Assign => "=",
            Token::Plus => "+",
            Token::Minus => "-",
            Token::Bang => "!",
            Token::Asterisk => "*",
            Token::Slash => "/",
            Token::LT => "<",
            Token::GT => ">",
            Token::Comma => ",",
            Token::Semicolon => ";",
            Token::LParen => "(",
            Token::RParen => ")",
            Token::LBrace => "{",
            Token::RBrace => "}",
            Token::Eq => "==",
            Token::NotEq => "!=",
            Token::Function => "fn",
            Token::Let => "let",
            Token::True => "true",
            Token::False => "false",
            Token::If => "if",
            Token::Else => "else",
            Token::Return => "return",
        }.to_owned()
    }
}

pub const KEYWORDS: Map<&'static str, Token> = phf_map! {
    "fn" => Token::Function,
    "let" => Token::Let,
    "true" => Token::True,
    "false" => Token::False,
    "if" => Token::If,
    "else" => Token::Else,
    "return" => Token::Return,
};

pub fn lookup_ident(ident: String) -> Token {
    KEYWORDS.get(&ident).unwrap_or(&Token::Ident(ident)).clone()
}
